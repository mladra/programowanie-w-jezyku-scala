import classes.IntIterator
import traits.{Cat, Dog, Filozof, Pet}

import scala.collection.mutable.ArrayBuffer

trait Equal {
  def isEqual(x: Any): Boolean
  def isNotEqual(x: Any): Boolean = !isEqual(x)
}

class Point(xc: Int, yc: Int) extends Equal {
  var x: Int = xc
  var y: Int = yc

  def isEqual(obj: Any) = obj.isInstanceOf[Point] && obj.asInstanceOf[Point].x == y
}

object TraitsDemo {
  def main(args: Array[String]) {

    // przykład trait
//    val p1 = new Point(2, 3)
//    val p2 = new Point(2, 4)
//    val p3 = new Point(3, 3)

//    println(p1.isNotEqual(p2))
//    println(p1.isNotEqual(p3))
//    println(p1.isNotEqual(2))
    // przykład z rozszerzaniem wielu traitow  - słowo kluczowe with
//    val dog = new Dog("Azor")
//    println(dog.name + dog.glos + dog.filozofia)
    // przykład z rozszerzeniem traitu w momencie tworzenia instancji klasy
//    val kotFilozof = new Cat("Filemon") with Filozof
//    kotFilozof.filozofia
    // przykład z generycznymi typami
//    val iterator = new IntIterator(10)
//    for (i <- 1 to 10) yield println(iterator.next())
    // przykład z przechowywaniem w kontenerze instancji różnych klas rozszerzających ten sam trait
//    val animals = ArrayBuffer.empty[Pet]
//    animals.append(dog)
//    animals.append(kotFilozof)
//    animals.foreach(pet => println(pet.name))
  }
}