package traits

trait Filozof {
  def filozofia() { println("Być albo nie być - o to jest pytanie") }
}
