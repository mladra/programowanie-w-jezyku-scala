package cake_pattern

import case_keyword.Computer

object Main {

  val computerServiceComponent: ComputerServiceComponent = new ComputerServiceComponentImpl with ComputerPrinterComponentImpl
  val computerService: computerServiceComponent.ComputerService = computerServiceComponent.computerService

  def main(args: Array[String]): Unit = {

    val computerService = this.computerService
    computerService.printComputer(Computer("Intel", "nVidia", null))

  }
}
