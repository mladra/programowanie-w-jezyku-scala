package cake_pattern

import case_keyword.Computer

trait ComputerServiceComponent {
  def computerService: ComputerService

  trait ComputerService {
    def printComputer(computer: Computer)
  }
}
