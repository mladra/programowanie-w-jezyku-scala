package cake_pattern
import case_keyword.Computer

trait ComputerPrinterComponentImpl extends ComputerPrinterComponent {

  def computerPrinter: ComputerPrinter = new ComputerPrinterImpl()

  class ComputerPrinterImpl() extends ComputerPrinter {
    def printComputer(computer: Computer): Unit = println(computer.processor + " : " + computer.graphicCard)
  }
}
