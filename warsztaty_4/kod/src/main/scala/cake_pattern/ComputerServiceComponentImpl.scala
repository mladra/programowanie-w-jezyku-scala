package cake_pattern

import case_keyword.Computer

trait ComputerServiceComponentImpl extends ComputerServiceComponent {
  this: ComputerPrinterComponent =>

  def computerService: ComputerService = new ComputerServiceImpl

  class ComputerServiceImpl extends ComputerService {
    def printComputer(computer: Computer): Unit = computerPrinter.printComputer(computer)
  }
}
