package cake_pattern

import case_keyword.Computer

trait ComputerPrinterComponent {
  def computerPrinter: ComputerPrinter

  trait ComputerPrinter {
    def printComputer(computer: Computer)
  }
}
