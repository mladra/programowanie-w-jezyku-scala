trait M[A] {
  def flatMap[B](f: A => M[B]): M[B]
}

//def unit[A](x: A): M[A]