package monads

/**
  * Created by macki on 27.03.2018.
  */
object MonadsDemo {
  def main(args: Array[String]): Unit = {

    val f = (i: Int) => List(i - 1, i, i + 1)

    val list = List(5, 6, 7)
    // metoda flatMap monady
    println(list.flatMap(f))

    // metoda unit monady
    val x = List.range(1, 10)
    x.apply(2)

  }
}
