package `implicit`

abstract class AbstractMonoid[A] {
  def add(x: A, y: A): A
  def unit: A
}
