package `implicit`

class IntMonoid extends AbstractMonoid[Int] {
  override def add(x: Int, y: Int): Int = x + y
  override def unit: Int = 0
}
