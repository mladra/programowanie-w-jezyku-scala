package `implicit`

class StringMonoid extends AbstractMonoid[String] {
  override def add(x: String, y: String): String = x concat y
  override def unit: String = ""
}
