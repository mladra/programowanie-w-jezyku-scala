package `sealed`

sealed trait Color
object Red extends Color
object Yellow extends Color
object Green extends Color