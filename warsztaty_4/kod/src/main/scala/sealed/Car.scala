package `sealed`

object Car {
  def react(color: Color) = color match {
    case Red => "stop"
    case Yellow => "slow"
    case Green => "continue"
  }
}