import `sealed`.{Car, Red}

object SealedDemo {
  def main(args: Array[String]): Unit = {

    println(Car.react(Red))
    // próba rozszerzenia sealed trait
//    case object Monsoon extends Season
  }

}
