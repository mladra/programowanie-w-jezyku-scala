import case_keyword.{Computer, ComputerPart, Device, Notebook}
import org.scalatest._

class CaseClassSpec extends FeatureSpec with GivenWhenThen {

  feature("Creation of case classes") {
    scenario("Creating with new keyword") {
      Given("created computer object with keyword")
      val comp = new Computer("Intel", "nVidia", null)

      When("computer has been created")

      Then("comp object must be not null adn have proper field values")
      assert(comp != null)
      assert(comp.processor == "Intel")
      assert(comp.graphicCard == "nVidia")
    }
    scenario("Creating without new keyword") {
      Given("created computer object without new keyword")
      val comp = Computer("Intel", "nVidia", null)

      When("computer has been created")

      Then("comp object must be not null adn have proper field values")
      assert(comp != null)
      assert(comp.processor == "Intel")
      assert(comp.graphicCard == "nVidia")
    }
  }

//  feature("Proving that fields in case classes are vals") {
//    Given("Created computer object")
//    val comp = case_keyword.Computer("Intel", "nVidia")
//
//    When("tried to change one of the fields")
//    comp.processor = "AMD"
//  }

  feature("Case class comparison") {
    scenario("Compare equals objects") {
      Given("two similar computer class objects")
      val comp1 = Computer("Intel", "nVidia", null)
      val comp2 = Computer("Intel", "nVidia", null)

      When("objects are compared")

      Then("they are equal but have different references")
      assert(comp1 == comp2) //compare object structure
      assert(comp1 ne comp2) //compare references
    }
  }

  feature("Case class copy") {
    scenario("Create case class copy using copy method") {
      Given("created computer")
      val comp = Computer("Intel", "nVidia", null)

      When("copy it")
      val comp2 = comp.copy()

      Then("assert copies")
      assert(comp == comp2)
      assert(comp ne comp2)
    }
    scenario("Create case class copy method and modify fields") {
      Given("created computer")
      val comp = Computer("Intel", "nVidia", null)

      When("copy it and change values")
      val comp2 = comp.copy(processor = "AMD")

      Then("assert copies")
      assert(comp2.processor == "AMD")
      assert(comp2.graphicCard == "nVidia")
    }
    scenario("Create case class copy method and show that it's shallow copy") {
      Given("created computer with devices")
      val part = new ComputerPart("Mouse")
      val comp = Computer("Intel", "nVidia", part)

      When("copy it and changed computer part name")
      val comp2 = comp.copy()

      assert(comp2 == comp)
      assert(comp.computerPart.name == "Mouse")

      part.name = "Keyboard"

      Then("change computer part")
      assert(comp.computerPart.name == "Keyboard")
      assert(comp2.computerPart.name == comp.computerPart.name)
    }
  }

  feature("Match examples") {
    scenario("Simple match example") {
      Given("number")
      val x: Int = 2
      var value: String = "undefined"

      When("match")
      x match {
        case 0 => value = "zero"
        case 1 => value = "one"
        case 2 => value = "two"
        case _ => value = "not recognized"
      }

      Then("check if correct")
      assert(value == "two")
    }
    scenario("Case class matching") {
      Given("case_keyword.Notebook object")
      val notebook: Device = Notebook("Lenovo")
      var msg = "simple message"

      When("match for type")
      notebook match {
        case Computer(_, _, _) => msg = "case_keyword.Computer"
        case Notebook(_) => msg = "case_keyword.Notebook"
        case _ => msg = "unknown"
      }

      Then("assert msg")
      assert(msg == "case_keyword.Notebook")
    }
    scenario("Only type matching") {
      Given("created computer")
      val computer: Device = Computer("AMD", "AMD", null)
      var msg = "simple message"

      When("match device")
      computer match {
        case c: Computer => msg = c.processor + " : " + c.graphicCard
        case n: Notebook => msg = n.manufacturer
      }

      Then("assert msg")
      assert(msg == "AMD : AMD")
    }
  }
}
