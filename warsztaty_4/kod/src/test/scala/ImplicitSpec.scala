import `implicit`.{IntMonoid, AbstractMonoid, StringMonoid}
import org.scalatest._

class ImplicitSpec extends FeatureSpec with GivenWhenThen {

  feature("Show how implicit keyword works") {
    scenario("Show implicit keyword") {
      Given("two implicit monolid values and method")
      implicit val stringMonoid: StringMonoid = new StringMonoid
      implicit val intMonoid: IntMonoid = new IntMonoid

      def sum[A](xs: List[A])(implicit m: AbstractMonoid[A]): A =
        if (xs.isEmpty) m.unit
        else m.add(xs.head, sum(xs.tail))

      When("Call method with different arguments")
      val s = sum(List("a", "b", "c"))(stringMonoid)
      val i = sum(List(1, 2, 3))

      Then("Assert computed values")
      assert(s == "abc")
      assert(i == 6)
    }
  }

}
