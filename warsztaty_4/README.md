# Programowanie w języku SCALA - warsztaty 4

Opracowanie zagadnień na 4 zajęcia z programowania w języku SCALA.

Materiały zawierają informacje na temat następujących zagadnień:
  
  * traits,
  * sealed keyword,
  * case keyword,
  * implicits,
  * monads,
  * cake pattern.

Skład zespołu:

  * Michał Ladra
  * Michał Mackiewicz

Uczelnia: Politechnika Łódzka

Wydział: FTIMS