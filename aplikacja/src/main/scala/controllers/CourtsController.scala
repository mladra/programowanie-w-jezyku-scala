package controllers

import controllers.generic.TableControllerTrait
import controllers.main.MainController
import dto.CourtDto
import scalafx.beans.property.StringProperty
import scalafx.collections.ObservableBuffer
import scalafx.scene.control.{SelectionMode, TableColumn, TableView}
import utils.Routes

class CourtsController extends TableControllerTrait[CourtDto] {

  val NAME_COLUMN_NAME = "Name"
  val TYPE_COLUMN_NAME = "Type"
  val FILTER_TERMS_LABEL = "Filter available terms with selected courts"

  //FIXME: mladra: Remove this mock data after successful integration with external api
  val COURTS_MOCK = ObservableBuffer(
    new CourtDto("Old Trafford", "FOOTBALL"),
    new CourtDto("Golden State Arena", "BASKETBALL"),
    new CourtDto("Lodz Arena", "VOLEYBALL")
  )

  val column1 = new TableColumn[CourtDto, String](NAME_COLUMN_NAME)
  val column2 = new TableColumn[CourtDto, String](TYPE_COLUMN_NAME)

  column1.cellValueFactory = cdf => StringProperty(cdf.value.name)
  column2.cellValueFactory = cdf => StringProperty(cdf.value.courtType)

  override val columns = List(column1, column2)
  override val table = new TableView[CourtDto] {
    items = COURTS_MOCK
    selectionModel.apply.setSelectionMode(SelectionMode.Single)
  }
  override val selectionActionLabel: String = "Filter terms with selected courts"
  override def onMenuItemSelection(): Unit = {
    //TODO: mladra: Apply filter to terms view
    MainController.route(Routes.TERMS)
  }

}
