package controllers

import controllers.generic.TableControllerTrait
import dto.TermDto
import scalafx.beans.property.StringProperty
import scalafx.collections.ObservableBuffer
import scalafx.scene.control.{SelectionMode, TableColumn, TableView}

class TermsController extends TableControllerTrait[TermDto] {

  //FIXME: mladra: Remove this mock data after successful integration with external api
  val TERMS_MOCK = ObservableBuffer(
    new TermDto(1, "date1", "start", "stop", "status1", "court1", "division1"),
    new TermDto(2, "date2", "start", "stop", "status2", "court2", "division2"),
    new TermDto(3, "date3", "start", "stop", "status3", "court3", "division3"),
    new TermDto(4, "date4", "start", "stop", "status4", "court4", "division4"),
  )

  val column1 = new TableColumn[TermDto, String]("Division")
  val column2 = new TableColumn[TermDto, String]("Court")
  val column3 = new TableColumn[TermDto, String]("Date")
  val column4 = new TableColumn[TermDto, String]("Start hour")
  val column5 = new TableColumn[TermDto, String]("End hour")

  column1.cellValueFactory = cdf => StringProperty(cdf.value.division)
  column2.cellValueFactory = cdf => StringProperty(cdf.value.court)
  column3.cellValueFactory = cdf => StringProperty(cdf.value.date)
  column4.cellValueFactory = cdf => StringProperty(cdf.value.startHour)
  column5.cellValueFactory = cdf => StringProperty(cdf.value.endHour)

  override val columns = List(column1, column2, column3, column4, column5)
  override val table = new TableView[TermDto] {
    items = TERMS_MOCK
    selectionModel.apply.setSelectionMode(SelectionMode.Single)
  }
  override val selectionActionLabel: String = "Reserve terms"
  override def onMenuItemSelection(): Unit = {
    //TODO: mladra: Send request to the server for reserving selected terms
    println("TermsController: onSelection method called")
  }

}
