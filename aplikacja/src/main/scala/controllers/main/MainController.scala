package controllers.main

import controllers._
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import utils.Routes

object MainController extends JFXApp {

  val WINDOW_TITLE = "WKS PAI Application"

  stage = new PrimaryStage {
    title = WINDOW_TITLE
    scene = new LoginController().loginScene()
  }

  def route(view: String): Unit = {
    view match {
      case Routes.HOME         => stage.scene = new HomeController().homeScene()
      case Routes.REGISTER     => stage.scene = new RegisterController().registerScene()
      case Routes.LOGIN        => stage.scene = new LoginController().loginScene()
      case Routes.COURTS       => stage.scene = new CourtsController().scene()
      case Routes.TERMS        => stage.scene = new TermsController().scene()
      case Routes.RESERVATIONS => stage.scene = new ReservationsController().scene()
    }
  }

}
