package controllers

import controllers.generic.TableControllerTrait
import dto.ReservationDto
import scalafx.beans.property.StringProperty
import scalafx.collections.ObservableBuffer
import scalafx.scene.control._

class ReservationsController extends TableControllerTrait[ReservationDto] {

  val COURT_COLUMN_NAME = "Court"
  val DATE_COLUMN_NAME = "Date"
  val STATUS_COLUMN_NAME = "Status"
  val START_HOUR_COLUMN_NAME = "Start hour"
  val END_HOUR_COLUMN_NAME = "End hour"

  //FIXME: mladra: Remove this mock data after successful integration with external api
  val RESERVATIONS_MOCK = ObservableBuffer(
    new ReservationDto(1, "user1", "status1", "date1", "start", "stop", "court1", 1),
    new ReservationDto(2, "user1", "status2", "date2", "start", "stop", "court2", 2),
    new ReservationDto(3, "user1", "status3", "date3", "start", "stop", "court3", 3),
    new ReservationDto(4, "user1", "status4", "date4", "start", "stop", "court4", 4)
  )

  val column1 = new TableColumn[ReservationDto, String](COURT_COLUMN_NAME)
  val column2 = new TableColumn[ReservationDto, String](DATE_COLUMN_NAME)
  val column3 = new TableColumn[ReservationDto, String](STATUS_COLUMN_NAME)
  val column4 = new TableColumn[ReservationDto, String](START_HOUR_COLUMN_NAME)
  val column5 = new TableColumn[ReservationDto, String](END_HOUR_COLUMN_NAME)

  column1.cellValueFactory = cdf => StringProperty(cdf.value.courtName)
  column2.cellValueFactory = cdf => StringProperty(cdf.value.date)
  column3.cellValueFactory = cdf => StringProperty(cdf.value.status)
  column4.cellValueFactory = cdf => StringProperty(cdf.value.startHour)
  column5.cellValueFactory = cdf => StringProperty(cdf.value.endHour)

  override val columns = List(column1, column2, column3, column4, column5)
  override val table = new TableView[ReservationDto] {
    items = RESERVATIONS_MOCK
    selectionModel.apply.setSelectionMode(SelectionMode.Single)
  }
  override val selectionActionLabel: String = "Cancel reservations"
  override def onMenuItemSelection(): Unit = {
    //TODO: mladra: Send request to the server for cancel selected reservations
    //TODO: mladra: Clickable should be only rows where reservation status is OPEN
    println("ReservationsController: onSelection method called")
  }

}
