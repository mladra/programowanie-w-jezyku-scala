package controllers

import controllers.main.MainController.stage
import controllers.generic.AbstractController
import controllers.main.MainController
import javafx.event.ActionEvent
import scalafx.geometry.{Insets, Pos}
import scalafx.scene.Scene
import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control._
import scalafx.scene.layout.GridPane
import scalafx.scene.paint.Color
import utils.Routes

class LoginController extends AbstractController {

  /* Labels */
  val USERNAME = "Username"
  val PASSWORD = "Password"
  val SIGN_IN = "Sign in"

  /* String constants */
  val ADMIN = "admin"
  val USER = "user"
  val SUCCESSFUL_LOGIN = "Successful login"
  val HELLO = "Hello "
  val CONTENT_SUCCESS = "Successful login. You can now make usage of system functions."
  val CONTENT_ERROR = "Check correctness of login and password, and try again..."
  val INVALID_CREDENTIALS = "Invalid credentials"
  val ERROR_HEADER = "Invalid username or password"
  val HOME = "Home"
  val REGISTER = "Register"

  /* Fields of view */
  val usernameLabel = new Label(USERNAME)
  val passwordLabel = new Label(PASSWORD)
  val usernameInput = new TextField() {
    promptText = USERNAME
  }
  val passwordInput = new PasswordField() {
    promptText = PASSWORD
  }
  val loginButton = new Button() {
    text = SIGN_IN
    alignmentInParent = Pos.CenterRight
    disable = true
    onAction = (event: ActionEvent) => login()
  }

  val registerButton = new Button() {
    text = REGISTER
    alignmentInParent = Pos.CenterRight
    disable = false
    onAction = (event: ActionEvent) => MainController.route(Routes.REGISTER)
  }

  /* Login stage */
  def loginScene(): Scene = {
    new Scene {
      fill = Color.LightBlue
      content = new GridPane() {
        hgap = 10
        vgap = 10
        padding = Insets(20)
        add(usernameLabel, FIRST_COL, FIRST_ROW)
        add(usernameInput, SECOND_COL, FIRST_ROW)
        add(passwordLabel, FIRST_COL, SECOND_ROW)
        add(passwordInput, SECOND_COL, SECOND_ROW)
        add(loginButton, SECOND_COL, THIRD_ROW)
        add(registerButton, FIRST_COL, THIRD_ROW)
      }
    }
  }

  /* Input validation and logic */
  var validUsername = false
  var validPassword = false

  usernameInput.text.onChange { (_, _, value) =>
    validUsername = !value.trim().isEmpty
    enableButton()
  }

  passwordInput.text.onChange { (_, _, value) =>
    validPassword = !value.trim().isEmpty
    enableButton()
  }

  def enableButton(): Unit = {
    loginButton.disable = !(validPassword & validUsername)
  }

  def showSuccessLoginDialog(login: String): Option[ButtonType] = {
    val alert = new Alert(AlertType.Information) {
      initOwner(stage)
      title = SUCCESSFUL_LOGIN
      headerText = HELLO + login
      contentText = CONTENT_SUCCESS
    }
    alert.showAndWait
  }

  def showInvalidCredentialsDialog: Option[ButtonType] = {
    val alert = new Alert(AlertType.Error) {
      initOwner(stage)
      title = INVALID_CREDENTIALS
      headerText = ERROR_HEADER
      contentText = CONTENT_ERROR
    }
    alert.showAndWait
  }

  def login(): Unit = {
    //TODO: mladra: Add sending server request for authorization token and some loading graphics during waiting
    // Temporary login, only for testing purposes
    if (usernameInput.text.value.equals(ADMIN) && passwordInput.text.value.equals(ADMIN)) {
      showSuccessLoginDialog(ADMIN)
      MainController.route(Routes.HOME)
    } else if (usernameInput.text.value.equals(USER) && passwordInput.text.value.equals(USER)) {
      showSuccessLoginDialog(USER)
      MainController.route(Routes.HOME)
    } else {
      showInvalidCredentialsDialog
    }
  }
}
