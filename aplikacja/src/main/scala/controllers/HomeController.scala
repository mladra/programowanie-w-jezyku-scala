package controllers

import controllers.generic.AbstractController
import controllers.main.MainController
import javafx.event.ActionEvent
import scalafx.geometry.{Insets, Pos}
import scalafx.scene.Scene
import scalafx.scene.control.{Button, Label}
import scalafx.scene.layout.GridPane
import scalafx.scene.paint.Color.LightBlue
import utils.Routes

class HomeController extends AbstractController {

  /* String constants */
  val COURTS = "Courts"
  val TERMS = "Terms"
  val RESERVATIONS = "Reservations"
  val LOGOUT = "Logout"
  val MENU = "Menu:"

  /* Labels */
  val menuLabel = new Label {
    text = MENU
    alignmentInParent = Pos.Center
    style = "-fx-font-weight: bold"
  }

  /* Buttons */
  val courtsButton = new Button {
    text = COURTS
    alignmentInParent = Pos.Center
    onAction = (event: ActionEvent) => goToCourts()
    prefWidth = 250.0D
  }

  val termsButton = new Button {
    text = TERMS
    alignmentInParent = Pos.Center
    onAction = (event: ActionEvent) => goToTerms()
    prefWidth = 250.0D
  }

  val reservationsButton = new Button {
    text = RESERVATIONS
    alignmentInParent = Pos.Center
    onAction = (event: ActionEvent) => goToReservations()
    prefWidth = 250.0D
  }

  val logoutButton = new Button {
    text = LOGOUT
    alignmentInParent = Pos.Center
    onAction = (event: ActionEvent) => logout()
    prefWidth = 250.0D
  }

  def homeScene(): Scene = {
    new Scene {
      fill = LightBlue
      content = new GridPane() {
        hgap = 10
        vgap = 10
        padding = Insets(20)
        add(menuLabel, FIRST_COL, FIRST_ROW)
        add(termsButton, FIRST_COL, SECOND_ROW)
        add(courtsButton, FIRST_COL, THIRD_ROW)
        add(reservationsButton, FIRST_COL, FOURTH_ROW)
        add(logoutButton, FIRST_COL, FIFTH_ROW)
      }
    }
  }

  def goToCourts(): Unit = {
    //TODO: mladra: Send request for all court types in database
    MainController.route(Routes.COURTS)
  }

  def goToTerms(): Unit = {
    //TODO: mladra: Send requst for all available terms in database
    MainController.route(Routes.TERMS)
  }

  def goToReservations(): Unit = {
    //TODO: mladra: Send request for all user reservations in database
    MainController.route(Routes.RESERVATIONS)
  }

  def logout(): Unit = {
    //TODO: mladra: Remove token from memory
    MainController.route(Routes.LOGIN)
  }
}
