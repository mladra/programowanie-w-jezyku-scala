package controllers.generic


import controllers.main.MainController
import javafx.event.ActionEvent
import javafx.scene.control.TableColumn
import scalafx.scene.Scene
import scalafx.scene.control.{Menu, MenuBar, MenuItem, TableView}
import scalafx.scene.layout.BorderPane
import scalafx.scene.paint.Color
import utils.Routes

import scala.collection.TraversableOnce

trait TableControllerTrait[T] {

  /* CONSTANTS */
  val VIEW_LABEL = "View"
  val BACK_LABEL = "Back"
  val SELECTION_LABEL = "Selection"

  /* VALUES TO OVERRIDE */
  val table: TableView[T]
  val columns: TraversableOnce[TableColumn[T, _]]
  val selectionActionLabel: String
  def onMenuItemSelection(): Unit

  def scene(): Scene = {
    table.columns ++= columns
    new Scene() {
      fill = Color.LightBlue
      root = new BorderPane {
        top = new MenuBar {
          useSystemMenuBar = true
          minWidth = 100
          menus.add(createBackMenuOption())
          menus.add(createSelectionMenuOption())
        }
        center = table
      }
    }
  }

  def createBackMenuOption(): Menu = {
    new Menu() {
      text = VIEW_LABEL
      items = List(
        new MenuItem {
          text = BACK_LABEL
          onAction = (e: ActionEvent) => MainController.route(Routes.HOME)
        }
      )
    }
  }

  def createSelectionMenuOption(): Menu = {
    new Menu() {
      text = SELECTION_LABEL
      items = List(
        new MenuItem {
          text = selectionActionLabel
          onAction = (e:ActionEvent) => onMenuItemSelection()
        }
      )
    }
  }

}
