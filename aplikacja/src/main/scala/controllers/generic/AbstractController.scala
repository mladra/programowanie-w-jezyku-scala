package controllers.generic

abstract class AbstractController {

  /* Grid constants */
  /* COLUMNS */
  val FIRST_COL = 0
  val SECOND_COL = 1
  val THIRD_COL = 2
  val FOURTH_COL = 3
  val FIFTH_COL = 4

  /* ROWS */
  val FIRST_ROW = 0
  val SECOND_ROW = 1
  val THIRD_ROW = 2
  val FOURTH_ROW = 3
  val FIFTH_ROW = 4
  val SIXTH_ROW = 5
  val SEVENTH_ROW = 6
  val EIGHTH_ROW = 7

}
