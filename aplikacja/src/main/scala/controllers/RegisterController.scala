package controllers

import java.util.regex.Pattern

import controllers.main.MainController.stage
import controllers.generic.AbstractController
import controllers.main.MainController
import dto.RegistrationDataDto
import javafx.event.ActionEvent
import javafx.scene.control.Tooltip
import scalafx.geometry.{Insets, Pos}
import scalafx.scene.Scene
import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control._
import scalafx.scene.layout.GridPane
import scalafx.scene.paint.Color.LightBlue
import utils.{Routes, StringUtils}


class RegisterController extends AbstractController {

  /* CONSTANTS */
  val EMAIL = "Email"
  val USERNAME = "Username"
  val PASSWORD = "Password"
  val CONFIRM_PASSWORD = "Confirm password"
  val FIRST_NAME = "First name"
  val SURNAME = "Surname"
  val REGISTER = "Register"
  val BACK_TO_LOGIN = "Back to login screen"

  /* CONSTRAINTS */
  val EMAIL_REGEX = "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$"
  val PERSONAL_DATA_MIN_LENGTH = 2
  val PERSONAL_DATA_MAX_LENGTH = 16
  val USERNAME_MIN_LENGTH = 4
  val USERNAME_MAX_LENGTH = 16
  val PASSWORD_MIN_LENGTH = 8
  val PASSWORD_MAX_LENGTH = 16

  /* ERROR MESSAGES */
  val INVALID_EMAIL = "Invalid email"
  val INVALID_USERNAME = "This field must have between 4 and 16 characters"
  val INVALID_PASSWORD = "This field must have between 8 and 16 characters"
  val INVALID_FIRST_NAME = "This field must have at least 2 characters"
  val INVALID_SURNAME = "This field must have at least 2 characters"
  val PASSWORDS_DONT_MATCH = "Password do not match"
  val INVALID_REGISTRATION_DATA = "Error during registration data validation"
  val INVALID_REGISTRATION_HEADER = "Invalid field's inputs"
  val CHECK_FIELDS_CORRECTNESS = "Check fields correctness and try again"

  /* LABELS */
  val emailLabel = new Label(EMAIL)
  val usernameLabel = new Label(USERNAME)
  val passwordLabel = new Label(PASSWORD)
  val confirmPasswordLabel = new Label(CONFIRM_PASSWORD)
  val firstNameLabel = new Label(FIRST_NAME)
  val surnameLabel = new Label(SURNAME)

  /* INPUTS */
  val usernameInput = new TextField() { promptText = USERNAME }
  val emailInput = new TextField() { promptText = EMAIL }
  val passwordInput = new PasswordField() { promptText = PASSWORD }
  val confirmPasswordInput = new PasswordField() { promptText = CONFIRM_PASSWORD }
  val firstNameInput = new TextField() { promptText = FIRST_NAME }
  val surnameInput = new TextField() { promptText = SURNAME }

  /* BUTTONS */
  val registerButton = new Button() {
    text = REGISTER
    alignmentInParent = Pos.Center
    onAction = (event: ActionEvent) => register()
    prefWidth = 250.0D
  }

  val backToLoginButton = new Button() {
    text = BACK_TO_LOGIN
    alignmentInParent = Pos.Center
    onAction = (event: ActionEvent) => backToLogin()
    prefWidth = 250.0D
  }

  def registerScene(): Scene = {
    new Scene {
      fill = LightBlue
      content = new GridPane() {
        hgap = 10
        vgap = 10
        padding = Insets(20)
        add(emailLabel, FIRST_COL, FIRST_ROW)
        add(usernameLabel, FIRST_COL, SECOND_ROW)
        add(passwordLabel, FIRST_COL, THIRD_ROW)
        add(confirmPasswordLabel, FIRST_COL, FOURTH_ROW)
        add(firstNameLabel, FIRST_COL, FIFTH_ROW)
        add(surnameLabel, FIRST_COL, SIXTH_ROW)

        add(emailInput, SECOND_COL, FIRST_ROW)
        add(usernameInput, SECOND_COL, SECOND_ROW)
        add(passwordInput, SECOND_COL, THIRD_ROW)
        add(confirmPasswordInput, SECOND_COL, FOURTH_ROW)
        add(firstNameInput, SECOND_COL, FIFTH_ROW)
        add(surnameInput, SECOND_COL, SIXTH_ROW)

        add(registerButton, FIRST_COL, SEVENTH_ROW, 2, 1)
        add(backToLoginButton, FIRST_COL, EIGHTH_ROW, 2, 1)
      }
    }
  }

  def register(): Unit = {
    val registrationData = new RegistrationDataDto(
      usernameInput.getText.trim,
      emailInput.getText.trim,
      passwordInput.getText.trim,
      confirmPasswordInput.getText.trim,
      firstNameInput.getText.trim,
      surnameInput.getText.trim
    )

    if (isValid(registrationData)) {
      sendRegisterRequest(registrationData)
    } else {
      showInvalidRegistrationDataDialog
    }
  }

  def isValid(registrationData: RegistrationDataDto): Boolean = {
    var result = true

    result = isEmailValid(registrationData, result)
    result = isUsernameValid(registrationData, result)
    result = isPasswordValid(registrationData, result)
    result = isConfirmPasswordValid(registrationData, result)
    result = arePasswordsSame(registrationData, result)
    result = isFirstNameValid(registrationData, result)
    result = isSurnameValid(registrationData, result)

    result
  }

  def isEmailValid(registrationData: RegistrationDataDto, result: Boolean): Boolean = {
    if (StringUtils.isEmpty(registrationData.email) || !Pattern.matches(EMAIL_REGEX, registrationData.email)) {
      showErrorMsg(emailInput, INVALID_EMAIL)
      false
    } else {
      clear(emailInput)
      result
    }
  }

  def isUsernameValid(registrationData: RegistrationDataDto, result: Boolean): Boolean = {
    if (StringUtils.isEmpty(registrationData.username) || registrationData.username.length < USERNAME_MIN_LENGTH || registrationData.username.length > USERNAME_MAX_LENGTH) {
      showErrorMsg(usernameInput, INVALID_USERNAME)
      false
    } else {
      clear(usernameInput)
      result
    }
  }

  def isPasswordValid(registrationData: RegistrationDataDto, result: Boolean): Boolean = {
    if (!isPasswordValid(registrationData.password)) {
      showErrorMsg(passwordInput, INVALID_PASSWORD)
      false
    } else {
      clear(passwordInput)
      result
    }
  }

  def isConfirmPasswordValid(registrationData: RegistrationDataDto, result: Boolean): Boolean = {
    if (!isPasswordValid(registrationData.confirmPassword)) {
      showErrorMsg(confirmPasswordInput, INVALID_PASSWORD)
      false
    } else {
      clear(confirmPasswordInput)
      result
    }
  }

  def arePasswordsSame(registrationData: RegistrationDataDto, result: Boolean): Boolean = {
    if (!StringUtils.areEqual(registrationData.password, registrationData.confirmPassword)) {
      showErrorMsg(passwordInput, PASSWORDS_DONT_MATCH)
      showErrorMsg(confirmPasswordInput, PASSWORDS_DONT_MATCH)
      false
    } else {
      result
    }
  }

  def isFirstNameValid(registrationData: RegistrationDataDto, result: Boolean): Boolean = {
    if (!isPersonalDataValid(registrationData.firstName)) {
      showErrorMsg(firstNameInput, INVALID_FIRST_NAME)
      false
    } else {
      clear(firstNameInput)
      result
    }
  }

  def isSurnameValid(registrationData: RegistrationDataDto, result: Boolean): Boolean = {
    if (!isPersonalDataValid(registrationData.surname)) {
      showErrorMsg(surnameInput, INVALID_SURNAME)
      false
    } else {
      clear(surnameInput)
      result
    }
  }

  def isPasswordValid(password: String): Boolean = {
    StringUtils.isNotEmpty(password) && password.length >= PASSWORD_MIN_LENGTH && password.length <= PASSWORD_MAX_LENGTH
  }

  def isPersonalDataValid(data: String): Boolean = {
    StringUtils.isEmpty(data) || (data.length >= PERSONAL_DATA_MIN_LENGTH && data.length <= PERSONAL_DATA_MAX_LENGTH)
  }

  def showErrorMsg(field: TextField, msg: String): Unit = {
    field.setStyle("-fx-text-box-border: red; -fx-focus-color: red;")
    field.setTooltip(new Tooltip(msg))
  }

  def clear(field: TextField): Unit = {
    field.setStyle(null)
    field.setTooltip(null)
  }

  def sendRegisterRequest(registrationData: RegistrationDataDto): Unit = {
    //TODO: mladra: Send request to the server with registration data
    //TODO: mladra: On positive response route to login screen with proper info
    //TODO: mladra: On negative response show message and/or mark invalid fields
    System.err.println("Method sendRegisterRequest(registrationData: RegistrationDataDto) not implemented...")
  }

  def showInvalidRegistrationDataDialog: Option[ButtonType] = {
    val alert = new Alert(AlertType.Error) {
      initOwner(stage)
      title = INVALID_REGISTRATION_DATA
      headerText = INVALID_REGISTRATION_HEADER
      contentText = CHECK_FIELDS_CORRECTNESS
    }
    alert.showAndWait
  }

  def backToLogin(): Unit = {
    MainController.route(Routes.LOGIN)
  }
}
