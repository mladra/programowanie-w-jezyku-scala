package utils

object StringUtils {

  def isEmpty(value: String): Boolean = {
    value == null || value.isEmpty
  }

  def isNotEmpty(value: String): Boolean = {
    !isEmpty(value)
  }

  def areEqual(value1: String, value2: String): Boolean = {
    value1.equals(value2)
  }

}
