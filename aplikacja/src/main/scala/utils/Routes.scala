package utils

object Routes {

  val LOGIN        = "Login"
  val REGISTER     = "Register"
  val HOME         = "Home"
  val COURTS       = "Courts"
  val TERMS        = "Terms"
  val RESERVATIONS = "Reservations"

}
