package dto

class ReservationDto(var reservationId: Long, var login: String, var status: String, var date: String,
                     var startHour: String, var endHour: String, var courtName: String, var termId: Long)
                     extends TableDtoTrait