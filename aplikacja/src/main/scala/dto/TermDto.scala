package dto

class TermDto(var id: Long, var date: String, var startHour: String, var endHour: String,
              var termStatus: String, var court: String, var division: String) extends TableDtoTrait