package dto

class RegistrationDataDto(var username: String, var email: String, var password: String, var confirmPassword: String,
                          var firstName: String, var surname: String)